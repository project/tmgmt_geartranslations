# TMGMT GearTranslations (tmgmt_geartranslations)

TMGMT GearTranslations module is a plugin for Translation Management Tool module (tmgmt).
It uses [GearTranslations translation platform](https://clients.geartranslations.com) for human
translation, allowing you to translate your site's content.

## Requirements

This module requires [TMGMT](http://drupal.org/project/tmgmt) module
to be installed.

Also you will need to enter a valid API key. You can obtain it by contacting
drupal@geartranslations.com.
